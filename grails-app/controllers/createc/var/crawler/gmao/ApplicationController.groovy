package createc.var.crawler.gmao

import grails.converters.JSON
import grails.core.GrailsApplication
import grails.plugins.*

class ApplicationController implements PluginManagerAware {

    GrailsApplication grailsApplication
    GrailsPluginManager pluginManager
    CreatecService createcService

    def index() {
        [grailsApplication: grailsApplication, pluginManager: pluginManager]
    }

    def createcTest() {
        render createcService.lastVariableValue([1,2,25]) as JSON
    }

    def createcTest2() {
        render createcService.allVariables() as JSON
    }
}
