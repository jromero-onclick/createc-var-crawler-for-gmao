package createc.var.crawler.gmao

import grails.gorm.transactions.Transactional

import java.lang.reflect.Field

class VariableRepositoryService {

    private Map<String, VariableConfig> varMap = [:]

    def add(VariableType variableType, String name, Long gmaoID, Long createcID, String periodValueName, String periodCustom) {
        Field field =  PeriodValue.getDeclaredField(periodValueName)
        Long periodValue = null
        if(field.type.name.equals("java.lang.String")) {
           periodCustom = PeriodValue."${periodValueName}"
        } else if(field.type.name.equals("long")) {
            periodValue = PeriodValue."${periodValueName}"
        }
        VariableConfig variableConfig = new VariableConfig(variableType: variableType, name: name, gmaoID: gmaoID, createcID: createcID, periodValue: periodValue, periodCustom: periodCustom)
        varMap.put(variableConfig.name, variableConfig)
    }

    def get(String name) {
        varMap.get(name)
    }

    def remove(String varName) {
        varMap.remove(varName)
    }

    List<VariableConfig>  getVarsForPeriodValue(Long periodValue) {
        varMap.values().findAll { VariableConfig variableConfig ->
            variableConfig.periodValue == periodValue
        }
    }

    List<VariableConfig> getVarsForPeriodCustom(String periodCustom) {
        varMap.values().findAll { VariableConfig variableConfig ->
            variableConfig.periodCustom == periodCustom
        }
    }

    List<VariableConfig> getVarsForGmaoIds(List<Long> gmaoIds) {
        varMap.values().findAll { VariableConfig variableConfig ->
            gmaoIds.contains(variableConfig.gmaoID)
        }
    }

    List<String> getVarNames() {
        varMap.keySet().toList()
    }
}
