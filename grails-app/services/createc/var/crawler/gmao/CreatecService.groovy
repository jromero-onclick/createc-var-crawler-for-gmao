package createc.var.crawler.gmao

import createc.var.crawler.gmao.client.createc.LastVariableValue
import createc.var.crawler.gmao.client.createc.Variable
import createc.var.crawler.gmao.client.createc.parser.CreatecParser
import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import groovy.transform.CompileStatic
import groovy.transform.CompileDynamic
import org.apache.commons.lang.exception.ExceptionUtils
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONElement
import org.springframework.web.client.ResourceAccessException

@CompileStatic
class CreatecService implements GrailsConfigurationAware {

    String apiUrl
    Integer apiCallAttempts
    CounterRepository createcNoSuccessCount

    static class CreatecServiceException extends RuntimeException {
        CreatecServiceException(String msg) {
            super(msg)
        }
    }


    @Override
    void setConfiguration(Config config) {
        apiUrl = config.getProperty("app.createc.host")
        apiCallAttempts = config.getProperty("app.createc.callAttempts") ? config.getProperty("app.createc.callAttempts").toInteger() : 3
    }

    @CompileDynamic
    LastVariableValue lastVariableValue(Long createcId) {
        log.info("Consultant lectura de parametre amb id ${createcId} a CREATEC")
        RestBuilder rest = new RestBuilder()
        String url = "${apiUrl}/variables/{id}/values/last"
        Map<String, Object> urlParams = [id: createcId]

        try {
            RestResponse restResponse = rest.get(url) {
                urlVariables urlParams
            }

            if(restResponse.statusCode.value() == 200 && restResponse.json) {
                JSONArray resultJson = restResponse.json
                if(resultJson.size()>0)
                    return CreatecParser.lastVariableValueFromJSONElement(resultJson.first())
            }
        } catch (ResourceAccessException raE) {
            log.error "No s'ha pogut connectar al servidor CREATEC"
            createcNoSuccessCount.increment()
            if(createcNoSuccessCount.count >= apiCallAttempts) {
                createcNoSuccessCount.reset()
                log.error "Enviar email"
                sendMail {
                    subject "CREATEC no respon ${new Date().format("yyyy-MM-dd HH:MM:SS")}"
                    text ExceptionUtils.getStackTrace(raE)
                }
            }
            return null
        }
        null
    }

    @CompileDynamic
    List<LastVariableValue> lastVariableValue(List<Long> createcIds) {
        log.info("Consultant lectura de parametres ${createcIds} a CREATEC")
        RestBuilder rest = new RestBuilder()
        String url = "${apiUrl}/variables/{ids}/values/last"
        Map<String, Object> urlParams = [ids: createcIds.join(",")]
        List<LastVariableValue> values = []

        try {
            RestResponse restResponse = rest.get(url) {
                urlVariables urlParams
            }
            if(restResponse.statusCode.value() == 200 && restResponse.json) {
                restResponse.json.each { JSONElement json ->
                    values << CreatecParser.lastVariableValueFromJSONElement(json)
                }
                return values
            }
        } catch (ResourceAccessException raE) {
            log.error "No s'ha pogut connectar al servidor CREATEC"
            createcNoSuccessCount.increment()
            if(createcNoSuccessCount.count >= apiCallAttempts) {
                createcNoSuccessCount.reset()
                log.error "Enviar email"
                sendMail {
                    subject "CREATEC no respon ${new Date().format("yyyy-MM-dd HH:MM:SS")}"
                    text ExceptionUtils.getStackTrace(raE)
                }
            }
            return values
        }
        null
    }

    @CompileDynamic
    List<Variable> allVariables() {
        log.info("Consultant llista parametres a CREATEC")
        RestBuilder rest = new RestBuilder()
        String url = "${apiUrl}/variables"
        List<Variable> variables = []

        try {
            RestResponse restResponse = rest.get(url)
            if(restResponse.statusCode.value() == 200 && restResponse.json) {
                restResponse.json.each { JSONElement json ->
                    variables << CreatecParser.variableFromJSONElement(json)
                }
                return variables
            }
        } catch (ResourceAccessException raE) {
            log.error "No s'ha pogut connectar al servidor CREATEC"
            createcNoSuccessCount.increment()
            if(createcNoSuccessCount.count >= apiCallAttempts) {
                createcNoSuccessCount.reset()
                log.error "Enviar email"
                sendMail {
                    subject "CREATEC no respon ${new Date().format("yyyy-MM-dd HH:MM:SS")}"
                    text ExceptionUtils.getStackTrace(raE)
                }
            }
            return variables
        }
        null
    }
}
