package createc.var.crawler.gmao

import createc.var.crawler.gmao.client.gmao.EquipmentInfo
import createc.var.crawler.gmao.client.gmao.MeterReading
import createc.var.crawler.gmao.client.gmao.parser.GmaoParser
import grails.config.Config
import grails.converters.JSON
import grails.core.support.GrailsConfigurationAware
import grails.gorm.transactions.Transactional
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.apache.commons.lang.exception.ExceptionUtils
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.ResourceAccessException

@CompileStatic
class GmaoService implements GrailsConfigurationAware {

    String apiUrl
    String apiUsername
    String apiPassword
    String apiTenantID
    Integer apiCallAttempts
    CounterRepository gmaoNoSuccessCount

    static class GmaoServiceException extends RuntimeException {
        GmaoServiceException(String msg) {
            super(msg)
        }
    }

    @Override
    void setConfiguration(Config config) {
        apiUrl = config.getProperty("app.gmao.host")
        apiUsername = config.getProperty("app.gmao.username")
        apiPassword = config.getProperty("app.gmao.password")
        apiTenantID = config.getProperty("app.gmao.tenantId")
        apiCallAttempts = config.getProperty("app.gmao.callAttempts") ? config.getProperty("app.gmao.callAttempts").toInteger() : 3
    }

    List<Long> pendingMaintenanceActionReadings() {
        log.info("Demanant lectures pendents")
        try {
            RestBuilder rest = new RestBuilder()
            String url = "${apiUrl}/api/meter/pending/?tenantId=${apiTenantID}"
            RestResponse restResponse = rest.get(url) {
                auth apiUsername, apiPassword
                header 'Accept', 'application/json'
                contentType "application/x-www-form-urlencoded"
            }
            if(restResponse.statusCode.value() == 200 && restResponse.json != null) {
                return GmaoParser.meterIdentifiersFromJSONElement(restResponse.json)
            } else {
                throw new GmaoServiceException("Error")
            }
        } catch (ResourceAccessException raE) {
            log.error("No s'ha pogut connectar al servidor GMAO")
            gmaoNoSuccessCount.increment()
            if(gmaoNoSuccessCount.count >= apiCallAttempts) {
                gmaoNoSuccessCount.reset()
                log.error "Enviar email"
                sendMail {
                    subject "GMAO no respon ${new Date().format("yyyy-MM-dd HH:MM:SS")}"
                    text ExceptionUtils.getStackTrace(raE)
                }
            }
            return null
        }
        null
    }

    @CompileDynamic
    EquipmentInfo registerEquipmentStatus(Long gmaoEquipmentId, Boolean workingStatus, Date statusTaken) {
        log.info("Guardant estat equip a GMAO")
        try {
            RestBuilder rest = new RestBuilder()
            String url = "${apiUrl}/api/equipment/status/${gmaoEquipmentId}?tenantId=${apiTenantID}"
            MultiValueMap<String,String> form = new LinkedMultiValueMap<String,String>()
            form.add("status", workingStatus.toString())
            form.add("statusTaken", statusTaken.format("yyyy-MM-dd'T'HH:mm:ss'Z'"))
            RestResponse restResponse = rest.put(url) {
                auth apiUsername, apiPassword
                header 'Accept', 'application/json'
                contentType "application/x-www-form-urlencoded"
                body form
            }
            if(restResponse.statusCode.value() == 200 && restResponse.json) {
                return GmaoParser.equipmentInfoFromJSONElement(restResponse.json)
            } else {
                throw new GmaoServiceException("Error")
            }
        } catch (ResourceAccessException raE) {
            log.error("No s'ha pogut connectar al servidor GMAO")
            gmaoNoSuccessCount.increment()
            if(gmaoNoSuccessCount.count >= apiCallAttempts) {
                gmaoNoSuccessCount.reset()
                log.error "Enviar email"
                sendMail {
                    subject "GMAO no respon ${new Date().format("yyyy-MM-dd HH:MM:SS")}"
                    text ExceptionUtils.getStackTrace(raE)
                }
            }
            return null
        }
        null
    }

    @CompileDynamic
    MeterReading registerDoubleReading(Long gmaoMeterId, Double valueRead, Date readenDate) {
        log.info("Guardant mesura a GMAO")
        try {
            RestBuilder rest = new RestBuilder()
            String url = "${apiUrl}/api/meterReading/?tenantId=${apiTenantID}"
            MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>()
            form.add("meter.id", gmaoMeterId.toString())
            form.add("reading", valueRead.toString())
//            form.add("readingUOM", "C")
            form.add("readingTaken", readenDate.format("yyyy-MM-dd'T'HH:mm:ss'Z'"))
            RestResponse restResponse = rest.post(url) {
                auth apiUsername, apiPassword
                header 'Accept', 'application/json'
                contentType "application/x-www-form-urlencoded"
                body form
            }
            if(restResponse.statusCode.value() == 200 && restResponse.json) {
                return GmaoParser.meterReadingFromJSONElement(restResponse.json)
            }
        } catch (ResourceAccessException raE) {
            log.error("No s'ha pogut connectar al servidor GMAO")
            gmaoNoSuccessCount.increment()
            if(gmaoNoSuccessCount.count >= apiCallAttempts) {
                gmaoNoSuccessCount.reset()
                log.error "Enviar email"
                sendMail {
                    subject "GMAO no respon ${new Date().format("yyyy-MM-dd HH:MM:SS")}"
                    text ExceptionUtils.getStackTrace(raE)
                }
            }
            return null
        }
        null
    }
}
