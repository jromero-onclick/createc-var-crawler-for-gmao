package createc.var.crawler.gmao

import createc.var.crawler.gmao.client.createc.LastVariableValue
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.apache.commons.validator.Var
import org.springframework.scheduling.annotation.Scheduled

@Slf4j
@CompileStatic
class ReadVariablesJobService implements GroovyInterceptable {

    boolean lazyInit = false
    VariableRepositoryService variableRepositoryService
    CreatecService createcService
    GmaoService gmaoService

    def invokeMethod(String name, args) {
        System.out.println("Called method $name")
        def metaMethod = metaClass.getMetaMethod(name, args)
        metaMethod.invoke(this, args)
    }

    @Scheduled(fixedDelay = PeriodValue.EVERY_5MINUTE, initialDelay = PeriodValue.EVERY_5MINUTE)
    def executePendingMaintenanceActions() {
        log.info("INICI - PENDING ACTIONS")
        def meterIdentifierList = gmaoService.pendingMaintenanceActionReadings()
        def varList = variableRepositoryService.getVarsForGmaoIds(meterIdentifierList)
        doRequestToServer(varList)
        log.info("Fi - PENDING ACTIONS")
    }

    @Scheduled(fixedDelay = PeriodValue.EVERY_5MINUTE, initialDelay = PeriodValue.EVERY_5MINUTE)
    def executeEvery5Minute() {
        log.info("INICI - READING EVERY 5 MINUTE")
        def varList = variableRepositoryService.getVarsForPeriodValue(PeriodValue.EVERY_5MINUTE)
        doRequestToServer(varList)
        log.info("FI - READING EVERY 5 MINUTE")
    }

    @Scheduled(fixedDelay = PeriodValue.EVERY_MINUTE, initialDelay = PeriodValue.EVERY_MINUTE)
    def executeEveryMinute() {
        log.info("INICI - READING EVERY MINUTE")
        def varList = variableRepositoryService.getVarsForPeriodValue(PeriodValue.EVERY_MINUTE)
        doRequestToServer(varList)
        log.info("FI - READING EVERY MINUTE")
    }

    @Scheduled(fixedDelay = PeriodValue.EVERY_15MINUTE, initialDelay = PeriodValue.EVERY_15MINUTE)
    def executeEvery15Minute() {
        log.info("INICI - READING EVERY 15 MINUTE")
        def varList = variableRepositoryService.getVarsForPeriodValue(PeriodValue.EVERY_15MINUTE)
        doRequestToServer(varList)
        log.info("FI - READING EVERY 15 MINUTE")
    }

    @Scheduled(fixedDelay = PeriodValue.EVERY_30MINUTE, initialDelay = PeriodValue.EVERY_30MINUTE)
    def executeEvery30Minute() {
        log.info("INICI - READING EVERY 30 MINUTE")
        def varList = variableRepositoryService.getVarsForPeriodValue(PeriodValue.EVERY_30MINUTE)
        doRequestToServer(varList)
        log.info("FI - READING EVERY 30 MINUTE")
    }

    @Scheduled(fixedDelay = PeriodValue.EVERY_HOUR, initialDelay = PeriodValue.EVERY_HOUR)
    def executeEveryHour() {
        log.info("INICI - READING EVERY HOUR")
        def varList = variableRepositoryService.getVarsForPeriodValue(PeriodValue.EVERY_HOUR)
        doRequestToServer(varList)
        log.info("FI - READING EVERY HOUR")
    }

    @Scheduled(cron = PeriodValue.EVERY_NOON)
    def executeEveryNoon() {
        log.info("INICI - READING EVERY NOON")
        def varList = variableRepositoryService.getVarsForPeriodCustom(PeriodValue.EVERY_NOON)
        doRequestToServer(varList)
        log.info("FI - READING EVERY NOON")
    }

    @Scheduled(cron = PeriodValue.EVERY_MIDNIGHT)
    def executeEveryMidnight() {
        log.info("INICI - READING EVERY MIDNIGHT")
        def varList = variableRepositoryService.getVarsForPeriodCustom(PeriodValue.EVERY_MIDNIGHT)
        doRequestToServer(varList)
        log.info("FI - READING EVERY MIDNIGHT")
    }

    def doRequestToServer(List<VariableConfig> variableConfigList) {
        def nomVariables = variableConfigList.collect { it.name }
        log.info( "Variables a consultar a CREATEC: ${nomVariables}")
        if(variableConfigList.size()) {
            List<LastVariableValue> lastVariableValues = createcService.lastVariableValue(variableConfigList.collect {
                it.createcID
            }.unique())

            log.info "Resultat de la consulta a CREATEC"
            lastVariableValues.each { LastVariableValue lastVariableValue ->
                List<VariableConfig> variableMappings = variableConfigList.findAll { it.createcID == lastVariableValue.variable.id }
                variableMappings.each { VariableConfig variableMapping ->
                    if(variableMapping.variableType == VariableType.MEASURE) {
                        log.info "Registrant reading... a GMAO per gmaoID: ${variableMapping.gmaoID}"
                        gmaoService.registerDoubleReading(variableMapping.gmaoID, lastVariableValue.valueDouble, lastVariableValue.date)
                    } else if(variableMapping.variableType == VariableType.STATUS) {
                        log.info "Registrant status... a GMAO per gmaoID: ${variableMapping.gmaoID}"
                        gmaoService.registerEquipmentStatus(variableMapping.gmaoID, lastVariableValue.valueBoolean, lastVariableValue.date)
                    }
                }
            }
        }


    }
}
