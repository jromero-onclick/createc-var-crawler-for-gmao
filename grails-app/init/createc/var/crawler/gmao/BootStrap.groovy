package createc.var.crawler.gmao

import grails.core.GrailsApplication


class BootStrap {

    GrailsApplication grailsApplication
    VariableRepositoryService variableRepositoryService

    def init = { servletContext ->

        grailsApplication.config.app.measures?.each { String key, it ->
            variableRepositoryService.add(
                    VariableType.MEASURE,
                    key.toString(),
                    Long.parseLong(it.gmao_id.toString()),
                    Long.parseLong(it.createc_id.toString()),
                    it.period.toString(), it.periodCustom.size()?:null)
        }

        grailsApplication.config.app.statuses?.each { String key, it ->
            variableRepositoryService.add(
                    VariableType.STATUS,
                    key.toString(),
                    Long.parseLong(it.gmao_id.toString()),
                    Long.parseLong(it.createc_id.toString()),
                    it.period.toString(), it.periodCustom.size()?:null)
        }

        println variableRepositoryService.getVarNames()

    }
    def destroy = {
    }
}
