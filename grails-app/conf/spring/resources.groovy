import createc.var.crawler.gmao.CounterRepository

// Place your Spring DSL code here
beans = {
    createcNoSuccessCount(CounterRepository)
    gmaoNoSuccessCount(CounterRepository)
}
