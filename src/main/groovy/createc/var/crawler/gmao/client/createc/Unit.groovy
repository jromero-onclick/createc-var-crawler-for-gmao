package createc.var.crawler.gmao.client.createc

import groovy.transform.CompileStatic

@CompileStatic
class Unit {
    Long id
    String name
    Integer min
    Integer max
}
