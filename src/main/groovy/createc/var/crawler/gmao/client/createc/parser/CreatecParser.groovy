package createc.var.crawler.gmao.client.createc.parser

import createc.var.crawler.gmao.client.createc.LastVariableValue
import createc.var.crawler.gmao.client.createc.Unit
import createc.var.crawler.gmao.client.createc.Variable
import createc.var.crawler.gmao.client.createc.VariableValueType
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.grails.web.json.JSONElement

@CompileStatic
class CreatecParser {

    @CompileDynamic
    static Unit unitsFromJSONElement(JSONElement json) {
        Unit units = new Unit()
        if(json.id) {
            units.id = json.id as Long
        }
        if(json.name) {
            units.name = json.name as String
        }
        if(json.min) {
            units.min = json.min as Integer
        }
        if(json.max) {
            units.max = json.max as Integer
        }
        units
    }

    @CompileDynamic
    static Variable variableFromJSONElement(JSONElement json) {
        Variable variable = new Variable()
        if(json.id) {
            variable.id = json.id as Long
        }
        if(json.name) {
            variable.name = json.name as String
        }
        if(json.type) {
            variable.type = json.type as String
        }
        if(json.historicEnabled) {
            variable.historicEnabled = json.historicEnabled == 1
        }
        if(json.levelReadAccess) {
            variable.levelReadAccess = json.levelReadAccess as Integer
        }
        if(json.levelWriteAccess) {
            variable.levelWriteAccess = json.levelWriteAccess as Integer
        }
        if(json.units) {
            variable.units = unitsFromJSONElement(json.units)
        }
        if(json.graphicable) {
            variable.graphicable = json.graphicable as Boolean
        }
        if(json.parameterType) {
            variable.parameterType = json.parameterType as String
        }
        if(json.nameReal) {
            variable.nameReal = json.nameReal as String
        }
        variable
    }

    @CompileDynamic
    static LastVariableValue lastVariableValueFromJSONElement(JSONElement json) {
        LastVariableValue lastVariableValue = new LastVariableValue()

        if(json.id) {
            lastVariableValue.id = json.id as Long
        }
        if(json.date) {
            lastVariableValue.date = new Date(json.date)
        }
        if(json.variable) {
            lastVariableValue.variable = variableFromJSONElement(json.variable)
        }
        if(json.valueString) {
            lastVariableValue.valueString = json.valueString as String
        }
        if(json.valueInteger) {
            lastVariableValue.valueInteger = json.valueInteger as Integer
        }
        if(json.valueDouble) {
            lastVariableValue.valueDouble = json.valueDouble as Double
        }
        if(json.valueBoolean) {
            lastVariableValue.valueBoolean = json.valueBoolean as Boolean
        }
        if(json.type) {
            lastVariableValue.type = VariableValueType.typeWithString(json.type.toString())
        }
        if(json.valueVariant) {
            switch(lastVariableValue.type) {
                case VariableValueType.STRING:
                    lastVariableValue.valueVariant = json.valueVariant as String
                    break
                case VariableValueType.INTEGER:
                    lastVariableValue.valueInteger = json.valueInteger as Integer
                    break
                case VariableValueType.DOUBLE:
                    lastVariableValue.valueDouble = json.valueDouble as Double
                    break
                case VariableValueType.BOOLEAN:
                    lastVariableValue.valueBoolean = json.valueBoolean as Boolean
                    break
                case VariableValueType.UNDEFINED:
                    lastVariableValue.valueVariant = null
                    break
            }
        }
        lastVariableValue
    }
}
