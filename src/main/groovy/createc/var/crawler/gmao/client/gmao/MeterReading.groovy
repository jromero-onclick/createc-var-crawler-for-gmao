package createc.var.crawler.gmao.client.gmao

import groovy.transform.CompileStatic

@CompileStatic
class MeterReading {
    Long meterId
    Double reading
    String readingUOM
    Date readingTaken
}
