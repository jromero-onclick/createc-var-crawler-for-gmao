package createc.var.crawler.gmao.client.createc

import groovy.transform.CompileStatic

@CompileStatic
class Variable {
    Long id
    String name
    String type
    Boolean historicEnabled
    Integer levelReadAccess
    Integer levelWriteAccess
    Unit units
    Boolean graphicable
    String parameterType
    String nameReal
}
