package createc.var.crawler.gmao.client.gmao

class EquipmentInfo {
    Long id
    String code
    String name
    String reference
    String identityCode
    String comments
    Boolean isStopped
}
