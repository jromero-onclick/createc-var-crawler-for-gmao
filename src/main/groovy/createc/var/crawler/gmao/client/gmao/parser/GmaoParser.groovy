package createc.var.crawler.gmao.client.gmao.parser

import createc.var.crawler.gmao.client.gmao.EquipmentInfo
import createc.var.crawler.gmao.client.gmao.MeterReading
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONElement

@CompileStatic
class GmaoParser {

    static List<Long> meterIdentifiersFromJSONElement(JSONElement json) {
        JSONArray jsonArray = (JSONArray)json
        List<Long> meterIdentifiers = []
        jsonArray.each {
            meterIdentifiers.push(it as Long)
        }
        meterIdentifiers
    }

    @CompileDynamic
    static MeterReading meterReadingFromJSONElement(JSONElement json) {
        MeterReading meterReading = new MeterReading()
        if(json.id) {
            meterReading.id = json.id as Long
        }
        if(json.reading) {
            meterReading.reading = json.reading as Double
        }
        if(json.readingUOM) {
            meterReading.readingUOM = json.readingUOM as String
        }
        if(json.readingTaken) {
            meterReading.readingTaken = json.readingTaken as Date
        }
        meterReading
    }

    @CompileDynamic
    static EquipmentInfo equipmentInfoFromJSONElement(JSONElement json) {
        EquipmentInfo equipmentInfo = new EquipmentInfo()
        if(json.id) {
            equipmentInfo.id = json.id as Long
        }
        if(json.code) {
            equipmentInfo.code = json.code as String
        }
        if(json.name) {
            equipmentInfo.name = json.name as String
        }
        if(json.reference) {
            equipmentInfo.reference = json.reference as String
        }
        if(json.identityCode) {
            equipmentInfo.identityCode = json.identityCode as String
        }
        if(json.isStopped) {
            equipmentInfo.isStopped = json.isStopped as Boolean
        }
        equipmentInfo
    }
}
