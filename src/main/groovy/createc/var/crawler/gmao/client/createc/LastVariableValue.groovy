package createc.var.crawler.gmao.client.createc

import groovy.transform.CompileStatic

@CompileStatic
class LastVariableValue {
    Long id
    Date date
    Object valueVariant
    Variable variable
    String valueString
    Integer valueInteger
    Double valueDouble
    Boolean valueBoolean
    VariableValueType type
}
