package createc.var.crawler.gmao.client.createc

enum VariableValueType {
    STRING, INTEGER, DOUBLE, BOOLEAN, UNDEFINED

    static VariableValueType typeWithString(String str) {
        if(str) {
            if(str.toLowerCase() == 'string') {
                return VariableValueType.STRING
            } else if(str.toLowerCase() == 'integer') {
                return VariableValueType.INTEGER
            } else if(str.toLowerCase() == 'double') {
                return VariableValueType.DOUBLE
            } else {
                return VariableValueType.BOOLEAN
            }
        }
        return VariableValueType.UNDEFINED
    }
}