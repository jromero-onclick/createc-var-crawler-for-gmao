package createc.var.crawler.gmao

import groovy.transform.CompileStatic

@CompileStatic
class VariableConfig {

    String name
    Long gmaoID
    Long createcID
    Long periodValue
    String periodCustom
    VariableType variableType
}
