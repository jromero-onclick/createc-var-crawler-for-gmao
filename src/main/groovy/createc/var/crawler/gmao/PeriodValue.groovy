package createc.var.crawler.gmao

class PeriodValue {
    static final long NEVER = -1L
    static final long EVERY_MINUTE = 60000L
    static final long EVERY_5MINUTE = 300000L
    static final long EVERY_15MINUTE = 900000L
    static final long EVERY_30MINUTE = 1800000L
    static final long EVERY_HOUR = 3600000L
    static final String EVERY_NOON = "0 0 12 1/1 * ?"
    static final String EVERY_MIDNIGHT = "0 0 0 1/1 * ?"
}
