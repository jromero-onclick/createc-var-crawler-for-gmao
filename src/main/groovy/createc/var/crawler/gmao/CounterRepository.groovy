package createc.var.crawler.gmao

class CounterRepository {
    private int count

    CounterRepository() {
        count = 0
    }
    void increment() {
        count++
    }

    void decrement() {
        count--
    }

    int getCount() {
        count
    }

    void reset() {
        count = 0
    }
}
